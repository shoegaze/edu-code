#!/bin/bash

read -p "Идентификатор пользователя: "
N=${#REPLY}
M=12
Q=$((N**3 % 5))
P=$((N**2 % 6))
echo -e "M = $M\nN = $N\nQ = $Q\nP = $P"

RES_PASS=$(cat /dev/urandom | tr -dc 'a-z' | fold -w $((Q+1)) | head -n 1)
RES_PASS+=$(cat /dev/urandom | tr -dc 'A-Z' | fold -w $((P+1)) | head -n 1)
RES_PASS+=$(cat /dev/urandom | tr -dc '0-9' | fold -w $((M-P-Q-2)) | head -n 1)

echo -e "\nСгенерированный пароль: $RES_PASS"
