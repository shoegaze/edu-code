#!/bin/bash

# requires: bash coreutils bc

# I don't know if it actually works properly

get_ascii_code() { # result differs from the example by 64 for cyrillic symbols
	printf "%d\n" "'$(printf $1 | iconv -f utf-8 -t cp1251)"
}

get_binary() { # only useful for 8-bit values
	printf "%8b\n" $(echo "ibase=10; obase=2; $1" | bc) | sed 's^ ^0^g'
}

get_decimal() {
	echo "$((2#$1))"
}

calculate_xor() {
	echo "$(get_binary $(($(get_decimal $1) ^ $(get_decimal $2))))"
}

string_to_ascii() {
	local input=$1
	local ascii_arr
	for ((i = 0; i < ${#input}; i++)); do
		ascii_arr+=($(get_ascii_code "${input:$i:1}"))
	done
	echo "${ascii_arr[*]}"
}

ascii_to_binary() {
	local input=($1)
	local binary_arr
	for i in ${input[@]}; do
		binary_arr+=($(get_binary $i))
	done
	echo "${binary_arr[*]}"
}

main() {
	local input; read input
	local P=${#input}
	local ascii_codes=($(string_to_ascii "${input}"))
	local K=$(IFS=+; echo "$((${ascii_codes[*]}))")
	local MaxVal=255 A=9 B=11 C=256 T=(201)
	local T_binary=($(get_binary "${T}"))
	local KSumm binary_codes Y
	
	if [[ $K -le $MaxVal ]]; then
		KSumm=$K
	else
		KSumm=$((K % (MaxVal + 1)))
	fi
	echo "KSumm = ${KSumm}"

	binary_codes=($(ascii_to_binary "${ascii_codes[*]}"))
	
	local T_next=${T[0]}
	for ((i = 0; i < P; i++)); do
		T_next=$(((A * T_next + B) % C))
		T_binary+=($(get_binary $T_next))
		Y+=($(get_decimal $(calculate_xor ${binary_codes[$i]} ${T_binary[$i]})))
	done
	echo "SummKodBukvOtkr = $(($(IFS=+; echo "$((${Y[*]}))") % C))"
}

main
