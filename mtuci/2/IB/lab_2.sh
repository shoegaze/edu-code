#!/bin/bash

# requires: bash coreutils dialog

DIALOG_CANCEL=1
DIALOG_ESC=255

normal_exit() {
	clear
	echo "Программа завершила свою работу"
	exit
}

err_exit() {
	clear
	echo "Программа аварийно завершила свою работу" >&2
	exit 1
}

dialog_codes() {
	case $1 in
		$DIALOG_CANCEL) normal_exit;;
		$DIALOG_ESC) err_exit;;
	esac
}

get_hash() {
	local sha512_hash=$(echo "${1}" | sha512sum)
	echo "${sha512_hash%% *}"
}

write_userdata() {
	local userdata=($1)
	local filename="auth_res/${userdata[0]}.txt"
	echo "${userdata[0]}" > "${filename}"
	for i in "${userdata[@]:1}"; do
		echo "${i}" >> "${filename}"
	done
}

write_password() {
	local userdata
	mapfile -t userdata <<< $(<auth_res/$(get_hash $1).txt)
	userdata[1]=$2
	write_userdata "${userdata[*]}"
}

passwd_reset_screen() {
	local userdata
	mapfile -t userdata <<< $(<auth_res/$(get_hash $1).txt)
	exec 3>&1
	local inputdata=($(dialog --ok-label "Далее" --no-cancel --form "Смена пароля" 0 0 0 \
		"Текущий пароль" 1 1 "" 1 20 30 0 \
		"Новый пароль" 3 1 "" 3 20 30 0 \
		"Подтверждение" 4 1 "" 4 20 30 0 \
		2>&1 1>&3))
	local exit_status=$?
	exec 3>&-
	dialog_codes $exit_status

	if [[ ${#inputdata[@]} -eq 0 ]]; then
		user_menu $1
	elif [[ ${#inputdata[@]} -lt 3 ]]; then
		exec 3>&1
		dialog --ok-label "Назад" --msgbox "Пожалуйста, заполните все поля формы" 0 0
		local exit_status=$?
		exec 3>&-
		dialog_codes $exit_status
		passwd_reset_screen $1
	elif [[ ${#inputdata[@]} -eq 3 ]]; then
		if [[ $(get_hash "${inputdata[0]}") == "${userdata[1]}" ]]; then
			if [[ "${inputdata[1]}" == "${inputdata[2]}" ]]; then
				if [[ "${inputdata[1]}" != "${userdata[5]//[.]//}" ]] && \
					[[ "${inputdata[1]}" != "${userdata[5]//[/]/.}" ]]; then
					write_password $1 $(get_hash "${inputdata[1]}")
					exec 3>&1
					dialog --ok-label "Меню пользователя" --msgbox "Пароль изменён" 0 0
					local exit_status=$?
					exec 3>&-
					dialog_codes $exit_status
					user_menu $1
				else
					exec 3>&1
					dialog --ok-label "Назад" --msgbox "Новый пароль не должен совпадать с датой рождения" 0 0
					local exit_status=$?
					exec 3>&-
					dialog_codes $exit_status
					passwd_reset_screen $1
				fi
			else
				exec 3>&1
				dialog --ok-label "Назад" --msgbox "Пароли не совпадают" 0 0
				local exit_status=$?
				exec 3>&-
				dialog_codes $exit_status
				passwd_reset_screen $1
			fi
		else
			exec 3>&1
			dialog --ok-label "Назад" --msgbox "Неверный пароль" 0 0
			local exit_status=$?
			exec 3>&-
			dialog_codes $exit_status
			passwd_reset_screen $1
		fi
	else
		exec 3>&1
		dialog --ok-label "Назад" --msgbox "Не используйте пробел" 0 0
		local exit_status=$?
		exec 3>&-
		dialog_codes $exit_status
		passwd_reset_screen $1
	fi
}

user_menu() {
	exec 3>&1
	local selection=$(dialog --ok-label "Выбрать" --no-cancel \
		--menu "Меню пользователя $1" 0 0 0 \
		1 "Изменить пароль" \
		2 "Выход" \
		2>&1 1>&3)
	local exit_status=$?
	exec 3>&-
	dialog_codes $exit_status
	case $selection in
		1) passwd_reset_screen $1;;
		2) main_menu;;
		3) normal_exit;;
	esac
}

login_screen() {
	exec 3>&1
	local logindata=($(dialog --ok-label "Далее" --no-cancel --form "Данные входа" 0 0 0 \
		"Имя пользователя" 1 1 "" 1 20 30 0 \
		"Пароль" 2 1 "" 2 20 30 0 \
		2>&1 1>&3))
	local exit_status=$?
	exec 3>&-
	dialog_codes $exit_status

	if [[ ${#logindata[@]} -eq 0 ]]; then
		main_menu
	else
		if [[ ${#logindata[@]} -eq 1 ]]; then
			exec 3>&1
			dialog --ok-label "Назад" --msgbox "Пожалуйста, заполните все поля формы" 0 0
			local exit_status=$?
			exec 3>&-
			dialog_codes $exit_status
			login_screen
		elif [[ ${#logindata[@]} -eq 2 ]]; then
			local username_hash=$(get_hash "${logindata[0]}")
			local password_hash=$(get_hash "${logindata[1]}")
			if [[ -f "auth_res/${username_hash}.txt" ]]; then
				local userdata=($(<"auth_res/${username_hash}.txt"))
				if [[ "${password_hash}" == "${userdata[1]}" ]]; then
					user_menu "${logindata[0]}"
				else
					exec 3>&1
					dialog --ok-label "Назад" --msgbox "Неверный пароль" 0 0
					local exit_status=$?
					exec 3>&-
					dialog_codes $exit_status
					login_screen
				fi
			else
				exec 3>&1
				dialog --ok-label "Назад" --msgbox "Пользователя ${logindata[0]} не существует" 0 0
				local exit_status=$?
				exec 3>&-
				dialog_codes $exit_status
				login_screen
			fi
		else
			exec 3>&1
			dialog --ok-label "Назад" --msgbox "Не используйте пробел" 0 0
			local exit_status=$?
			exec 3>&-
			dialog_codes $exit_status
			login_screen
		fi
	fi
}

main_menu() {
	exec 3>&1
	local selection=$(dialog --ok-label "Выбрать" --no-cancel \
		--menu "Меню авторизации" 0 0 0 \
		1 "Вход" \
		2 "Регистрация" \
		3 "Выход" \
		2>&1 1>&3)
	local exit_status=$?
	exec 3>&-
	dialog_codes $exit_status
	case $selection in
		1) login_screen;;
		2) registration_screen;;
		3) normal_exit;;
	esac
}

registration_screen() {
	exec 3>&1
	local userdata=($(dialog --ok-label "Далее" --no-cancel --form "Регистрация пользователя" 0 0 0 \
		"Имя пользователя" 1 1 "" 1 20 30 0 \
		"Пароль" 2 1 "" 2 20 30 0 \
		"Фамилия" 3 1 "" 3 20 30 0 \
		"Имя" 4 1 "" 4 20 30 0 \
		"Отчество" 5 1 "" 5 20 30 0 \
		"Дата рождения" 6 1 "" 6 20 30 0 \
		"Место рождения" 7 1 "" 7 20 30 0 \
		"Телефон" 8 1 "" 8 20 30 0 \
		2>&1 1>&3))
	local exit_status=$?
	exec 3>&-
	dialog_codes $exit_status
	
	if [[ ${#userdata[@]} -le 8 ]]; then
		if [[ ${#userdata[@]} -eq 0 ]]; then
			main_menu
		elif [[ ${#userdata[@]} -eq 8 ]]; then
			if [[ "${userdata[1]}" == "${userdata[5]}" ]]; then
				exec 3>&1
				dialog --ok-label "Назад" --msgbox "Пароль не должен совпадать с датой рождения" 0 0
				local exit_status=$?
				exec 3>&-
				dialog_codes $exit_status
				registration_screen
			else
				local username="${userdata[0]}"
				userdata[0]=$(get_hash "${username}")
				if [[ -f "auth_res/${userdata[0]}.txt" ]]; then
					exec 3>&1
					dialog --ok-label "Назад" --msgbox "Пользователь ${username} уже зарегистрирован" 0 0
					local exit_status=$?
					exec 3>&-
					dialog_codes $exit_status
					main_menu
				else
					userdata[1]=$(get_hash "${userdata[1]}")
					write_userdata "${userdata[*]}"
					exec 3>&1
					dialog --ok-label "Меню" --msgbox "Пользователь ${username} успешно зарегистрирован" 0 0
					local exit_status=$?
					exec 3>&-
					dialog_codes $exit_status
					main_menu
				fi
			fi
		else
			exec 3>&1
			dialog --ok-label "Назад" --msgbox "Пожалуйста, заполните все поля формы" 0 0
			local exit_status=$?
			exec 3>&-
			dialog_codes $exit_status
			registration_screen
		fi
	else
		exec 3>&1
		dialog --ok-label "Назад" --msgbox "Не используйте пробел" 0 0
		local exit_status=$?
		exec 3>&-
		dialog_codes $exit_status
		registration_screen
	fi
}

if [[ ! -d "auth_res" ]]; then
	mkdir -p "auth_res"
fi

main_menu
